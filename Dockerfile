from node:9.4-alpine
workdir /app

env NODE_PATH=/usr/local/lib/node_modules

copy package.json /app/package.json
# copy package-lock.json /app/package-lock.json
run npm install

copy gulpfile.js index.js tsconfig.json /app/

expose 3000
entrypoint ["npm"]
