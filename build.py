from subprocess import check_call
import argparse

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument( "--version" )
  arg = parser.parse_args()

  print( "docker build -t registry.gitlab.com/foureyeslabs/static-web-pug-typescript ." )
  check_call( "docker build -t registry.gitlab.com/foureyeslabs/static-web-pug-typescript .", shell = True )
  print( "docker tag registry.gitlab.com/foureyeslabs/static-web-pug-typescript registry.gitlab.com/foureyeslabs/static-web-pug-typescript:%s" % arg.version )
  check_call( "docker tag registry.gitlab.com/foureyeslabs/static-web-pug-typescript registry.gitlab.com/foureyeslabs/static-web-pug-typescript:%s" % arg.version, shell = True )
  print( "docker push registry.gitlab.com/foureyeslabs/static-web-pug-typescript" )
  check_call( "docker push registry.gitlab.com/foureyeslabs/static-web-pug-typescript", shell = True )
  print( "docker push registry.gitlab.com/foureyeslabs/static-web-pug-typescript:%s" % arg.version )
  check_call( "docker push registry.gitlab.com/foureyeslabs/static-web-pug-typescript:%s" % arg.version, shell = True )
