# Static Web with Pug and Typescript

This project is intended to be used to help generate static web files. These
web files can then be hosted by a static web server such as Nginx, or put
onto a static web host like S3 + CloudFront.

## Package Structure

The application is expecting the following structure.

    view/
        [description] Holds the views. If file ends in .pug, it will use pug
            processor
    component/
        [description] Pug files that can be included. Here for separation.
    layout/
        [description] The layout Pug files. Here for separation.
    asset/
        js/
            [description] If file ends in .ts, it will use Typescript.
        css/
            [description]  If file ends in .scss, it will use Sass.
    dist/
        [description] The output folder of the compiled pages.

## Usage

The easiest way is to use a Docker compose file.

    version: '3'
    services:
      web:
        image: registry.gitlab.com/foureyeslabs/static-web-pug-typescript
        ports:
          - 3000:3000
        volumes:
          - ./view:/app/view
          - ./component:/app/component
          - ./layout:/app/layout
          - ./asset:/app/asset
          - ./dist:/app/dist
        command: run develop

This will automatically start running it. If you would like to start developing,
just run `docker-compose up`. If you would like to just build it, then do
`docker-compose run web run build`.

## Extensibility

This is designed to be extensible up to 1 level. This will run the pipe commands
for the files within the proper locations, and also prefixed with an _ locations.
