const gulp = require( 'gulp' );
const ts = require( 'gulp-typescript' );
const pug = require( 'gulp-pug' );
const express = require( 'gulp-express' );
const sass = require( 'gulp-sass' );
const yaml = require( 'node-yaml' );
const file = require( 'gulp-file' );
const rename = require( 'gulp-rename' );
const _ = require( "lodash" );

gulp.task( 'watch', ['type-scripts', "asset"], () => {
  express.run( ['./index.js'] );
  gulp.watch( ['asset/**/*.ts'], ['type-scripts'] );
  gulp.watch( ['asset/**/*.js'], ['static-js'] );
  gulp.watch( ['asset/**/*.scss'], ['sass'] );
  gulp.watch( ['view/**/*'], ['view'] );
  gulp.watch( ['layout/**/*'], ['view'] );
  gulp.watch( ['asset/**/*.ts', 'views/**/*.pug', 'asset/**/*.scss'], express.notify );
});

const tsProject = ts.createProject( 'tsconfig.json' );
const alsoPrefix = files => _.map( files, f => `_${f}` ).concat( files );

gulp.task( 'type-scripts', () => {
  try {
    const tsResult = tsProject.src()
    .pipe( tsProject() );
    return tsResult.js.pipe( gulp.dest( 'dist/js' ) );
  } catch (e) {
    console.error( e );
  }
});

gulp.task( 'settings', () => {
  const environment = process.env.CONFIG_ENVIRONMENT;

  return gulp.src( `asset/js/config.${environment}.js` )
    .pipe( rename( 'config.js' ) )
    .pipe( gulp.dest( 'dist/js' ) );
} );

gulp.task( 'sass', () => {
  return gulp.src( alsoPrefix( ["asset/**/*.scss"] ) )
    .pipe( sass().on( 'error', sass.logError ) )
    .pipe( gulp.dest( 'dist/css' ) );
});

gulp.task( 'view', () => {
  try {
    return gulp.src( alsoPrefix( ["view/**/*.pug", "layout/**/*.pug", "component/**/*.pug"] ) )
    .pipe( pug({
      basedir: "/app",
      locals: {
        "_": _
      }
    }) )
    .pipe( gulp.dest( 'dist' ) );
  } catch (e) {
    console.error( e );
  }
} );

gulp.task( 'media', () => {
  return gulp.src( alsoPrefix([ "asset/media/**/*" ]) )
      .pipe( gulp.dest( 'dist/media' ) );
} );

gulp.task( 'json', () => {
  return gulp.src( alsoPrefix([ "asset/js/**/*.json" ]) )
      .pipe( gulp.dest( 'dist/json' ) );
} );

gulp.task( 'static-js', () => {
  return gulp.src( alsoPrefix([ "asset/js/**/*" ]) )
      .pipe( gulp.dest( 'dist/js' ) );
} );

gulp.task( 'static-css', () => {
  return gulp.src( alsoPrefix([ "asset/css/**/*" ]) )
      .pipe( gulp.dest( 'dist/css' ) );
} );

gulp.task( 'asset', ['sass', 'view', 'media', 'static-js', 'static-css', 'json', 'settings'] );
gulp.task( "build", ["asset", "type-scripts"] );
gulp.task( "develop", ["watch"])
